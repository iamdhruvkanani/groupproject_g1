﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace GroupProject_G1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CustomerDetailsPage : Page
    {
        //coded by Roop
  
        private string name, phonNumber, orderTotal;
        public CustomerDetailsPage()
        {
            this.InitializeComponent();
        }
        /// <summary>
        /// Implemented OnNavigatedTo to receive data through the  NavigationEventArgs.Parameterargument .The following recieves a parameter order
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            float order = (float)e.Parameter;

            orderTotal = order.ToString();
            TxtOrderTotal.Text = "$ " + orderTotal;


        }

        /// <summary>
        /// Button Click Event. When Cancel Button is clicked control goes to MainPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BCancel(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }

        /// <summary>
        /// Button Click Event. On click of AddWaitingOrders the customer details get added to a list in MainPage
        /// The Ui navigates to MainPage when button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddWaitingOrder(object sender, RoutedEventArgs e)
        {

            name = TxtCustomerName.Text.ToString();
            phonNumber = TxtCustomerNumber.Text.ToString();
            // local variable string that is used to combine all the info 

            string combined;
            combined = "Name: " + name + " , " + "Phone: " + phonNumber + " , " + "Order Total: " + "$" + orderTotal;


            Frame.Navigate(typeof(MainPage), combined);

        }
    }
}
