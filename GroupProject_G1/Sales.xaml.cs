﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace GroupProject_G1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    //Coded By Jashanpreet
    public sealed partial class Sales : Page
    {
        public Sales()
        {
            this.InitializeComponent();
        }
        /// <summary>
        /// Implemented OnNavigatedTo to receive data through the  NavigationEventArgs.Parameter argument
        /// Destination page receives the business logic objects to work with through navigation methods
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            string order = (string)e.Parameter;
            if (order != null)
            {
                LstSales.Items.Add(order.ToString());
            }





        }
        /// <summary>
        /// On Click of Back Button. UI Navigats to MainPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void OnGoBack(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }
    }
}
