﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace GroupProject_G1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    //Coded By Roop
    public sealed partial class LoginPage : Page
    {
        public LoginPage()
        {
            this.InitializeComponent();
        }
        /// <summary>
        /// If any of username or password is empty the login fails  and page does not navigate to other page
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            if (string.IsNullOrEmpty(TxtUsername.Text))
            {
                e.Cancel = true;
            }
            else if (string.IsNullOrEmpty(PasswordBox.ToString()))
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Button Click Event. On clicking Login button the control goes to MainPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLogin(object sender, RoutedEventArgs e)
        {
            string userName = "admin";
            string password = "hello";
            string txtUserName = TxtUsername.Text.ToString();
            string txtpassword = PasswordBox.Password.ToString();

            if (txtUserName.Equals(userName) && txtpassword.Equals(password))
            {
                Frame.Navigate(typeof(MainPage));
            }
            else
            {
                ShowError("Username Or Password InCorrect. Try Again ! ");
            }
        }
        /// <summary>
        /// Dialog Box to show error
        /// </summary>
        /// <param name="errMessage"></param>
        private async void ShowError(string errMessage)
        {
            //Object Initializer Syntax
            ContentDialog dlg = new ContentDialog()
            {
                Content = errMessage,
                Title = "Incorrect Credentials !",
                PrimaryButtonText = "Ok",
            };
            await dlg.ShowAsync();
        }
    }
}
