﻿using Windows.System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using GroupProjectLibrary;
using System;
using Windows.Storage;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace GroupProject_G1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private string menuItem, numberOfItems, sales;
        //Lists for the various items 

        public List<Order> _dessertMenuList = new List<Order>();

        public List<Order> _pizzaMenuList = new List<Order>();


        public List<Order> _burgerMenuList = new List<Order>();

        public List<Order> _drinkMenuList = new List<Order>();
        public List<string> _orders = new List<string>();
        float hst;
        float bill;

        private float subTotal;

        public MainPage()
        {

            this.InitializeComponent();
            //added elements to the lists
            //Coded By Jashanpreet
            _dessertMenuList.Add(new Order("Cinammon Bites", 5.99f));
            _dessertMenuList.Add(new Order("Cheese Cake", 8.99f));
            _dessertMenuList.Add(new Order("Choclate Balls", 9.99f));
            _dessertMenuList.Add(new Order("Mango Ice-Cream", 7.99f));
            _dessertMenuList.Add(new Order("Choclate Ice-Cream", 10.99f));

            _burgerMenuList.Add(new Order("Cheese Burger", 5.99f));
            _burgerMenuList.Add(new Order("Spicy Chicken Burger", 8.99f));
            _burgerMenuList.Add(new Order("Whopper Burger", 9.99f));
            _burgerMenuList.Add(new Order("Veg Burger", 7.99f));
            _burgerMenuList.Add(new Order("Jr Chicken Burger", 10.99f));

            _pizzaMenuList.Add(new Order("Pepperoni", 5.99f));
            _pizzaMenuList.Add(new Order("Veg Pizza", 8.99f));
            _pizzaMenuList.Add(new Order("Hawaian Pizza", 9.99f));
            _pizzaMenuList.Add(new Order("Three Meat", 7.99f));
            _pizzaMenuList.Add(new Order("Canadian Pizza", 10.99f));

            _drinkMenuList.Add(new Order("Pepsi", 5.99f));
            _drinkMenuList.Add(new Order("Coke", 8.99f));
            _drinkMenuList.Add(new Order("Mountain Dew", 9.99f));
            _drinkMenuList.Add(new Order("Orange Juice", 7.99f));
            _drinkMenuList.Add(new Order("Apple Juice", 10.99f));


        }


        public List<Order> DessertList
        {
            get { return _dessertMenuList; }
        }
        public List<Order> BurgerList
        {
            get { return _burgerMenuList; }
        }
        public List<Order> PizzaList
        {
            get { return _pizzaMenuList; }
        }
        public List<Order> DrinkList
        {
            get { return _drinkMenuList; }
        }







        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            string order = (string)e.Parameter;
            if (order != null)
            {
                LstWaitingOrders.Items.Add(order.ToString());
            }

        }


        /// <summary>
        /// On click of Desserts button the Dessert list is displayed 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddDessertList(object sender, RoutedEventArgs e)
        {

            LstMenu.ItemsSource = DessertList;

        }


        /// <summary>
        /// Click Event.Clicking the Burger button displays Burgers lists and its respective items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddBurgerList(object sender, RoutedEventArgs e)
        {

            LstMenu.ItemsSource = BurgerList;
        }
        /// <summary>
        /// Click Event.Clicking the Pizza button displays Burgers lists and its respective items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void AddPizzaList(object sender, RoutedEventArgs e)
        {

            LstMenu.ItemsSource = PizzaList;

        }

        /// <summary>
        /// Click Event.Clicking the Drinks button displays Burgers lists and its respective items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddDrinkList(object sender, RoutedEventArgs e)
        {

            LstMenu.ItemsSource = DrinkList;
        }

        private void ClearOrder(object sender, RoutedEventArgs e)
        {

            LstBill.Items.Clear();
            TxtNumberOfItems.Text = "";
            TxtSubTotal.Text = "";
            TxtTax.Text = "";
            TxtTotal.Text = "";
            numberOfItems = LstBill.Items.Count.ToString();
            TxtNumberOfItems.Text = numberOfItems;
            subTotal = 0;
        }

        /// <summary>
        /// Click Event. Created a Hyperlink button that when clicked navigates to InfoPage and displays details about the cafeteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnInfoClick(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(InfoPage));
        }

        /// <summary>
        /// On Click Event. When FinishWaitingOrders button is pressed the orders in the waitingOrder list get cleared
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 


        //Coded By Dhruv

        private void FinishWaitingOrders(object sender, RoutedEventArgs e)
        {

            try
            {




                _orders.Add(LstWaitingOrders.SelectedItem.ToString());
                for (int i = 0; i < _orders.Count; i++) // Loop through List with for
                {
                    sales = _orders[i];
                    
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }


            try
            {
                string fileName = ApplicationData.Current.LocalFolder.Path + @"\Orders.csv";
                File.WriteAllLines(fileName, _orders);


            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
            LstWaitingOrders.Items.RemoveAt(LstWaitingOrders.SelectedIndex);

        }
        private async void ShowError(string errMessage)
        {
            //Object Initializer Syntax
            ContentDialog dlg = new ContentDialog()
            {
                Content = errMessage,
                Title = "Error encountered",
                PrimaryButtonText = "Ok",
            };
            await dlg.ShowAsync();
        }


        /// <summary>
        /// On Click Event. When an item is selected from the list and then Add button is clicked it gets
        /// added to OrderTotal List where the Total Bill including the taxes is calculated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BAdd(object sender, RoutedEventArgs e)
        {
            int i = 0;
            float total = 0.0f;
            List<float> tot1 = new List<float>();

            foreach (Order order in LstMenu.SelectedItems)
            {


                menuItem = LstMenu.SelectedItem.ToString();

                LstBill.Items.Add(menuItem);

                tot1.Add(total);

                foreach (Order x in LstMenu.SelectedItems)
                {
                    subTotal += x.Price;
                    total += subTotal;
                }
                hst = 0.13f * total;
                bill = total + hst;


                bill = (float)System.Math.Round(bill, 2);
                hst = (float)System.Math.Round(hst, 2);



                TxtTax.Text = "$ " + hst.ToString();
                TxtTotal.Text = "$ " + bill.ToString();

                TxtSubTotal.Text = "$ " + subTotal.ToString();

            }
            LstMenu.SelectedItem = null;





            numberOfItems = LstBill.Items.Count.ToString();
            TxtNumberOfItems.Text = numberOfItems;


        }

        /// <summary>
        /// When the page loads the amount of tax, bill and number of items remain null and values change accordingly when user selects something
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 








        // This Method will reset all Values on Page Load
        private void PageLoaded(object sender, RoutedEventArgs e)
        {
            LstBill.Items.Clear();
            TxtNumberOfItems.Text = "";
            TxtSubTotal.Text = "";
            TxtTax.Text = "";
            TxtTotal.Text = "";
            subTotal = 0;
        }


        /// <summary>
        /// Click event. When home button in TopBar is clicked it logouts from the current page and takes the UI back to LoginPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BLogout(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(LoginPage));
        }

        /// <summary>
        /// Click event. Clicking the Sales button navigates to the Sales Page and displays the TotaoOrders for a particular day.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BSales(object sender, RoutedEventArgs e)
        {
            try
            {
                Frame.Navigate(typeof(Sales), sales);

            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }


        }

        /// <summary>
        /// Click Event. Button click that navigates to CustomerDetailsPage and takes the bill details as argument
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BAddOrders(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(CustomerDetailsPage), bill);

        }


    }
}

