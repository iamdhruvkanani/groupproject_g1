﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GroupProjectLibrary
{
    // Coded by Jashan
    //This class manages orders 
    public class Order
    {
        public Order(string orderName, float price)
        {

            OrderName = orderName;

            Price = price;


        }

        public string OrderName { get; set; }
        public float Price { get; set; }
        public override string ToString()
        {
            return $"{OrderName} , ${Price}";
        }

    }
}