﻿using System;

namespace GroupProjectLibrary
{
    public class Customer
    {
        //coded by roop
        private string _custName;
        private int _custNumber;

        //constructor added 
        public Customer(string custName, int custNumber)
        {
            _custName = custName;
            _custNumber = custNumber;
        }
        //properties for respective customer details
        public string CustomerName
        {
            get
            {
                return _custName;
            }
            set
            {
                //   if (string.IsNullOrEmpty(value))
                //   {
                //       throw new Exception("Please Enter Name !");
                //   }
                _custName = value;
            }
        }

        public int CustomerNumber
        {
            get
            {
                return _custNumber;
            }
            set
            {
                //if(string.IsNullOrEmpty(value.ToString()))
                //{
                //    throw new Exception("Please Enter Number !");
                //}
                _custNumber = value;
            }
        }

    }
}
